package sample.mbg.step;

import static org.assertj.core.api.Assertions.*;
import static org.mybatis.dynamic.sql.SqlBuilder.*;
import static sample.mbg.mapper.TTab01DynamicSqlSupport.*;

import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import java.util.function.Function;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.io.Reader;
import java.lang.invoke.MethodHandle;
import java.lang.invoke.MethodHandles;
import java.lang.invoke.MethodType;

import com.thoughtworks.gauge.BeforeScenario;
import com.thoughtworks.gauge.BeforeSuite;
import com.thoughtworks.gauge.Step;
import com.thoughtworks.gauge.Table;
import com.thoughtworks.gauge.TableRow;
import com.thoughtworks.gauge.TableCell;
import com.thoughtworks.gauge.datastore.SuiteDataStore;

import sample.gauge.step.BaseSteps;
import org.mybatis.dynamic.sql.select.CountDSL.CountWhereBuilder;
import org.mybatis.dynamic.sql.delete.DeleteDSL.DeleteWhereBuilder;
import org.mybatis.dynamic.sql.select.QueryExpressionDSL.QueryExpressionWhereBuilder;
import org.mybatis.dynamic.sql.AndOrCriteriaGroup;
import org.mybatis.dynamic.sql.SqlBuilder;
// import org.mybatis.dynamic.sql.BindableColumn;
import org.mybatis.dynamic.sql.ExistsPredicate;
import org.mybatis.dynamic.sql.SqlColumn;
import org.mybatis.dynamic.sql.AbstractSingleValueCondition;

import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;

import org.apache.commons.lang3.StringUtils;

public class TTab01Steps /* extends BaseSteps<sample.mbg.model.TTab01, sample.mbg.mapper.TTab01Mapper> */ {

    SqlSession sess;
    sample.mbg.mapper.TTab01Mapper mapper;

    /**
     * gauge実行時に1回だけ実行する.
     * - DBセッションの取得とキャッシュ
     */
    @BeforeSuite
    public void beforeSuite() throws Exception {
        Reader r = Resources.getResourceAsReader("mybatis-config.xml");
        SqlSessionFactory factory = new SqlSessionFactoryBuilder().build(r);
        sess = factory.openSession();
        SuiteDataStore.put("session", sess);
        mapper = sess.getMapper(sample.mbg.mapper.TTab01Mapper.class);
    }

    /**
     * シナリオ実行前に毎回事項する.
     * - DBセッションの取得とキャッシュ
     * - マッパの取得
     */
    @BeforeScenario
    public void beforeScenario() throws Exception {
//        SuiteDataStore.put("cache", new HashMap<String, String>());
//        SuiteDataStore.put("tables", new HashMap<String, List<Object>>());
        sess = (SqlSession)SuiteDataStore.get("session");
        mapper = sess.getMapper(sample.mbg.mapper.TTab01Mapper.class);
    }

    /**
     * コンストラクタ
     */
    public TTab01Steps() throws Exception {
        // beforeSuite();
    }

    /**
     * テーブルの情報をキャッシュする
     * 検条件は少ないと想定されるためマージはしない.
     */
    @Step("T_TAB01: 次の検索条件をキャッシュする。 <table>")
    public void cache(Table table) throws Exception {
        //TODO: キャッシュの初期化はシナリオに移動する
        Map<String, List<List<Map<String, String>>>> caches = (Map<String, List<List<Map<String, String>>>>)SuiteDataStore.get("tables");
        if(caches == null){
            caches = new HashMap<String, List<List<Map<String, String>>>>();
            SuiteDataStore.put("tables", caches);
        }
        List<List<Map<String, String>>> cache = caches.get("TTab01");
        if(cache == null){
            cache = new ArrayList();
            caches.put("TTab01", cache);
        }

        // ヘッダ情報マップ
        List<Map<String, String>> conditionMap = null;

        for(TableRow tableRow : table.getTableRows()){
            System.out.println("** cache: called.");
            // ヘッダ情報マップが未設定の場合
            if(conditionMap == null){
                // ヘッダ情報マップの取得
                conditionMap = getConditionMap(tableRow);
                // 1行目は条件のため次へ
                continue;
            }
            cache.add(getMap(conditionMap, tableRow));
            System.out.printf("** cache.size()=[%d].\n", cache.size());
        }
        // SuiteDataStore.put("tables", caches);
    }

    /**
     * 列名と条件のマップのリストを返す
     */
    List<Map<String, String>> getConditionMap(TableRow tableRow){
        List<Map<String, String>> list = new ArrayList();
        for(TableCell tableCell : tableRow.getTableCells()){
            // 列名
            String columnName = tableCell.getColumnName();
            // 検索条件
            String condition = tableCell.getValue();
            list.add(new HashMap(){{
                put("columnName", columnName);
                put("condition", getConditionMethodName(condition));
            }});
        }
        return list;
    }

    /**
     * 条件メソッド名の取得
     */
    String getConditionMethodName(String condition){
        switch(condition){
            case "<":
                return "isLessThanWhenPresent";
            case "<=":
                return "isLessThanOrEqualToWhenPresent";
            case ">=":
                return "isGreaterThanOrEqualToWhenPresent";
            case ">":
                return "isGreaterThanWhenPresent";
            case "like":
                return "isLikeWhenPresent";
            case "not like":
                return "isNotLikeWhenPresent";
            case "null":
                return "isNull";
            case "not null":
                return "isNotNull";
            case "in":
                return "isIn";
            case "not in":
                return "isNotIn";
        }
        return "isEqualToWhenPresent";
    }

    /**
     * TableRowからMapのリストを返す
     */
    List<Map<String, String>> getMap(List<Map<String, String>> conditionMap, TableRow tableRow) throws Exception {
        Map<String, Map<String, String>> map2 = getMap2();
        List<Map<String, String>> list = new ArrayList();

        int idx = -1;
        for(TableCell tableCell : tableRow.getTableCells()){
            idx++;
            String condition = conditionMap.get(idx).get("condition"); 
            list.add(new HashMap(){{
                String columnName = tableCell.getColumnName();
                put("columnName", columnName);
                put("value", tableCell.getValue());
                put("condition", condition);
                put("className", map2.get(columnName).get("className"));
                put("fieldName", map2.get(columnName).get("fieldName"));
            }});
        }
        return list;
    }

    /**
     * MBGからの情報を取得する.
     * DB列名(D_NUM01)に紐づくJavaフィールド名(dNum01)、Javaクラス名(jara.math.BigDecimal)のマップ情報を取得する.
     * TODO:gauge実行時に上記の情報が取得できるようならそっちに置き換える
     */
    Map<String, Map<String, String>> getMap2(){
        Map<String, Map<String, String>> map = new HashMap();

        map.put("D_NUM01", new HashMap<String, String>(){{
            put("colName", "D_NUM01");
            put("fieldName", "dNum01");
            put("className", "java.math.BigDecimal");
        }});
        map.put("D_CHR01", new HashMap<String, String>(){{
            put("colName", "D_CHR01");
            put("fieldName", "dChr01");
            put("className", "java.lang.String");
        }});
        map.put("D_DAT01", new HashMap<String, String>(){{
            put("colName", "D_DAT01");
            put("fieldName", "dDat01");
            put("className", "java.util.Date");
        }});

        return map;
    }

    /**
     * CSVファイルの出力する
     * TODO: CSVファイルに出力する
     */
    @Step("T_TAB01: 出力する。")
    public void print() throws Exception {
        Map<String, List<List<Map<String, String>>>> caches = (Map<String, List<List<Map<String, String>>>>)SuiteDataStore.get("tables");
        List<List<Map<String, String>>> cache = caches.get("TTab01");

        System.out.printf("** cache.size()=[%d].\n", cache.size());
        System.out.println(cache);
        selectList.apply(cache).apply(mapper)
            .forEach(System.out::println)
        ;
    }

    /**
     * 検索条件とマッパを引数として結果一覧を返す関数.
     * @params conds TableCellから情報を取得した１行分のMap
     * @params mapper MSGで作成したマッパ
     * @return List 結果一覧
     * TODO: 1引数以外の条件にも対応する ※難問！？
     *  - m.invoke()の引数は配列で渡せるのか？
     *  - 引数の数によって戻り値の型が違うので明示的なキャストができない.分岐が必要なのか？
     */
    Function<List<List<Map<String, String>>>, Function<sample.mbg.mapper.TTab01Mapper, List<sample.mbg.model.TTab01>>> selectList = conditions-> mapper -> {
        List<sample.mbg.model.TTab01> list = mapper.select(dsl->{
            QueryExpressionWhereBuilder builder = dsl.where();
            MethodHandles.Lookup lookup = MethodHandles.lookup();
            try{

                for(List<Map<String, String>> row: conditions){
                    List<AndOrCriteriaGroup> cond= new ArrayList();
                    for(Map<String, String> c: row){
                        if(StringUtils.isEmpty(c.get("value"))) continue;

                        Field f = sample.mbg.mapper.TTab01DynamicSqlSupport.class.getDeclaredField(c.get("fieldName"));
                        SqlColumn<?> sqlColumn = (SqlColumn)f.get(null);

                        Method m = SqlBuilder.class.getDeclaredMethod(c.get("condition"), Object.class);
                        // Class<?> cls = Class.forName(c.get("className"));
                        Object val = myConv(c);
                        // Object result = m.invoke(null, val);

System.out.printf("** m.getRetrunType()=[%s], m.getParameterTypes()=[%s].\n", m.getReturnType(), .m.getParameterTypes());
                        MethodType methodType = MethodType.methodType(m.getReturnType(), m.getParameterTypes());
System.out.printf("** c.get(\"condition\")=[%s].\n", c.get("condition"));
                        MethodHandle mh = lookup.findStatic(SqlBuilder.class, c.get("condition"), methodType);
System.out.printf("** mh.type().returnType()=[%s].\n", mh.type().returnType());
                        // org.mybatis.dynamic.sql.where.condition.IsGreaterThanOrEqualTo r = (org.mybatis.dynamic.sql.where.condition.IsGreaterThanOrEqualTo)mh.invokeExact(val);
                        // Object r = mh.invoke(val);
/*
                        cond.add(and(
                            (SqlColumn)sqlColumn,
                            // (AbstractSingleValueCondition)m.invoke(null, val)
                            // mh.invokeExact(val)    //NG: error: no suitable method found for and(SqlColumn,Object)
                            // mh.type().returnType().cast(mh.invokeExact(val))    //NG: error: no suitable method found for and(SqlColumn,CAP#1)
                            r.getClass().cast(r)
                            // mh.type().returnType().cast(mh.invoke(null, "a"))    //NG: error: no suitable method found for and(SqlColumn,Object)
                        ));
*/
                    }
                    builder.or(cond);
                }
                return builder;
            }catch(Exception e){
                e.printStackTrace();
                return builder; 
            }catch(Throwable e){ 
                e.printStackTrace();
                return builder; 
            }
        });
        return list;
    };

    /**
     * クラス名に対応した型の値を取得する
     * TODO: swtich文に変更する
     */
    Object myConv(Map<String, String> map) throws Exception {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        String type = map.get("className");
        String value = map.get("value");

        return "java.lang.Short".equals(type) 
            ? Short.valueOf(value)
            : "java.lang.Integer".equals(type)
            ? Integer.valueOf(value)
            : "java.math.BigDecimal".equals(type)
            ? new BigDecimal(value)
            : "java.util.Date".equals(type)
            ? sdf.parse(value)
            : value
        ;
    }

}
