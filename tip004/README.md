# README

```
docker login registry.gitlab.com
docker pull registry.gitlab.com/tomate-broccoli/gitpod_tip011

oracle.env:
TZ=Asia/Tokyo
ORACLE_SID=ORCL
ORACLE_PDB=MYORCL
ORACLE_PWD=oracle21C3
ORACLE_CHARACTERSET=AL32UTF8
NLS_LANG=Japanese_Japan.AL32UTF8

docker run -d --env-file ./oracle.env --name ORCL2 -p 1521:1521 registry.gitlab.com/tomate-broccoli/gitpod_tip011

docker exec -it ORCL2 sqlplus system/oracle21C3@MYORCL
create user scott identified by tiger account unlock ;
grant connect, resource to scott ;
alter user scott quota UNLIMITED on users ;

connect scott/tiger@MYORCL
create table t_tab01 ( d_num01 number, d_chr01 varchar2(50), d_dat01 date) ;
insert into t_tab01 values(1, 'abc', sysdate);
insert into t_tab01 values(2, 'xyz', sysdate);
commit;

exit;
```

## gradle
```
$ gradle tasks
$ gradle clean build mbg
```

## gauge

```
$ cd app/
$ curl -SsL https://downloads.gauge.org/stable | sh
$ gauge init java
$ gradle clean gauge --info
```

