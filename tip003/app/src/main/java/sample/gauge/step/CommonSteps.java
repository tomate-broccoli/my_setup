package sample.gauge.step;

import java.io.Reader;
import java.math.BigDecimal;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;

import com.thoughtworks.gauge.BeforeScenario;
import com.thoughtworks.gauge.BeforeSuite;
import com.thoughtworks.gauge.Step;
import com.thoughtworks.gauge.Table;
import com.thoughtworks.gauge.TableCell;
import com.thoughtworks.gauge.TableRow;
import com.thoughtworks.gauge.datastore.SuiteDataStore;

public class CommonSteps {
   SqlSession sess;

   @BeforeSuite
   public void beforeSuite() throws Exception {
    Reader r = Resources.getResourceAsReader("mybatis-config.xml");
    SqlSessionFactory factory = new SqlSessionFactoryBuilder().build(r);
    sess = factory.openSession();
    SuiteDataStore.put("session", sess);
   }

   @BeforeScenario
   public void beforeScenario() throws Exception {
        SuiteDataStore.put("cache", new HashMap<String, String>());
        SuiteDataStore.put("tables", new HashMap<String, List<Object>>());
        sess = (SqlSession)SuiteDataStore.get("session");
   }

   @Step("処理を確定する。")
   public void commit () throws Exception {
        sess.commit();
   }

   @Step("次のデータをキャッシュする。 <table>")
   public void cache(Table table) throws Exception {
        Map<String, String> cache = (Map<String, String>)SuiteDataStore.get("cache");
        for(TableRow row : table.getTableRows()){
            for(TableCell cell : row.getTableCells()){
                String k = String.format("<%s>", cell.getColumnName());
                String v = cell.getValue();
               System.out.print(String.format("** cache: %s=[%s].\n", k, v));
                cache.put(k, v);
            }
        }
   }

   public static String getCachedValue(TableRow row, String keyword){
        Map<String, String> cache = (Map<String, String>)SuiteDataStore.get("cache");
        String str = row.getCell(keyword).startsWith("<") ? cache.get(row.getCell(keyword)) : row.getCell(keyword);
        System.out.print(String.format("** getCachedValue: %s=[%s].\n", keyword, str));
        return str;
   }

   public static String toStr(TableRow row, String keyword){
        return getCachedValue(row, keyword);
   }

   public static Short toShort(TableRow row, String keyword){
        String val = getCachedValue(row, keyword);
        return val == null ? null : Short.valueOf(val);
   }

   public static Long toLong(TableRow row, String keyword){
        String val = getCachedValue(row, keyword);
        return val == null ? null : Long.valueOf(val);
   }

   public static BigDecimal toBigDecimal(TableRow row, String keyword){
        String val = getCachedValue(row, keyword);
        return val == null ? null : new BigDecimal(val);
   }

   public static Date toDate(TableRow row, String keyword) throws Exception {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        String val = getCachedValue(row, keyword);
        return val == null ? null : sdf.parse(val);
   }

   @Step("CSVファイル出力先フォルダを<csvDir>に設定する。")
   public void setCsvDir(String csvDir) throws Exception {
       String dirPath = "csv/"+ csvDir;
       SuiteDataStore.put("csvDir", dirPath);
       Files.createDirectories(Paths.get(dirPath));
   }

}
