package sample.mbg.model;

import java.math.BigDecimal;
import java.util.Date;

public class Tab01 {
    private BigDecimal num01;

    private String chr01;

    private Date dat01;

    public BigDecimal getNum01() {
        return num01;
    }

    public void setNum01(BigDecimal num01) {
        this.num01 = num01;
    }

    public String getChr01() {
        return chr01;
    }

    public void setChr01(String chr01) {
        this.chr01 = chr01;
    }

    public Date getDat01() {
        return dat01;
    }

    public void setDat01(Date dat01) {
        this.dat01 = dat01;
    }
}