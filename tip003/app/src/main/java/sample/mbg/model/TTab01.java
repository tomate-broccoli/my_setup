package sample.mbg.model;

import java.math.BigDecimal;
import java.util.Date;

public class TTab01 {
    private BigDecimal dNum01;

    private String dChr01;

    private Date dDat01;

    public BigDecimal getdNum01() {
        return dNum01;
    }

    public void setdNum01(BigDecimal dNum01) {
        this.dNum01 = dNum01;
    }

    public String getdChr01() {
        return dChr01;
    }

    public void setdChr01(String dChr01) {
        this.dChr01 = dChr01;
    }

    public Date getdDat01() {
        return dDat01;
    }

    public void setdDat01(Date dDat01) {
        this.dDat01 = dDat01;
    }
}