# t_tab01.spec

    $ gradle clean gauge -PspecsDir=specs/t_tab01.spec -Ptags="setup" --info

## sample
Tags: setup

X T_TAB01: 次の検索条件をキャッシュする。

    | D_NUM01 | D_CHR01 | D_DAT01             | D_NUM01 | D_CHR01 |
    |---------|---------|---------------------|---------|---------|
    | >=      | like    | is not null         | in      | =       |
    |         |         |                     |         | abc     |

* T_TAB01: 次の検索条件をキャッシュする。

    | D_NUM01 | D_CHR01 | D_DAT01             | D_NUM01 | D_CHR01 |
    |---------|---------|---------------------|---------|---------|
    | >=      | like    | is not null         | in      | =       |
    | 0.5     | a%      |                     | 1, 2, 3 | abc     |

X T_TAB01: 次の検索条件をキャッシュする。

    | D_NUM01 | D_CHR01 | D_DAT01             |
    |---------|---------|---------------------|
    | >=      | like    | >=                  |
    | 0.5     |         | 2024/01/08 12:34:56 |

* T_TAB01: 出力する。
