package sample.mbg.step;

import static org.assertj.core.api.Assertions.*;
import static org.mybatis.dynamic.sql.SqlBuilder.*;
import static sample.mbg.mapper.TTab01DynamicSqlSupport.*;

import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Parameter;
import java.io.Reader;
import java.lang.invoke.MethodHandle;
import java.lang.invoke.MethodHandles;
import java.lang.invoke.MethodType;
import java.lang.reflect.Type;
import java.lang.reflect.ParameterizedType;

import com.thoughtworks.gauge.BeforeScenario;
import com.thoughtworks.gauge.BeforeSuite;
import com.thoughtworks.gauge.Step;
import com.thoughtworks.gauge.Table;
import com.thoughtworks.gauge.TableRow;
import com.thoughtworks.gauge.TableCell;
import com.thoughtworks.gauge.datastore.SuiteDataStore;

import sample.gauge.step.BaseSteps;
import org.mybatis.dynamic.sql.select.CountDSL.CountWhereBuilder;
import org.mybatis.dynamic.sql.delete.DeleteDSL.DeleteWhereBuilder;
import org.mybatis.dynamic.sql.select.QueryExpressionDSL.QueryExpressionWhereBuilder;
import org.mybatis.dynamic.sql.AndOrCriteriaGroup;
import org.mybatis.dynamic.sql.SqlBuilder;
// import org.mybatis.dynamic.sql.BindableColumn;
import org.mybatis.dynamic.sql.ExistsPredicate;
import org.mybatis.dynamic.sql.SqlColumn;
import org.mybatis.dynamic.sql.AbstractSingleValueCondition;
import org.mybatis.dynamic.sql.VisitableCondition;

import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;

import org.apache.commons.lang3.StringUtils;
import com.fasterxml.jackson.dataformat.csv.CsvMapper;
import com.fasterxml.jackson.dataformat.csv.CsvSchema;
import com.fasterxml.jackson.databind.SequenceWriter;
import java.io.BufferedWriter;
import java.nio.file.Files;
import java.nio.file.Paths;

// ${tableClass}:TTab01
public class TTab01Steps /* extends BaseSteps<sample.mbg.model.TTab01, sample.mbg.mapper.TTab01Mapper> */ {

    SqlSession sess;
    // ${mapperClass}:sample.mbg.mapper.TTab01Mapper
    sample.mbg.mapper.TTab01Mapper mapper;

    /**
     * gauge実行時に1回だけ実行する.
     * - DBセッションの取得とキャッシュ
     */
    @BeforeSuite
    public void beforeSuite() throws Exception {
        Reader r = Resources.getResourceAsReader("mybatis-config.xml");
        SqlSessionFactory factory = new SqlSessionFactoryBuilder().build(r);
        sess = factory.openSession();
        SuiteDataStore.put("session", sess);
        // ${mapperClass}:sample.mbg.mapper.TTab01Mapper
        mapper = sess.getMapper(sample.mbg.mapper.TTab01Mapper.class);
    }

    /**
     * シナリオ実行前に毎回事項する.
     * - DBセッションの取得とキャッシュ
     * - マッパの取得
     */
    @BeforeScenario
    public void beforeScenario() throws Exception {
        //NOTE: @BeforeSuiteで取得しているのでコメントにする.
        // sess = (SqlSession)SuiteDataStore.get("session");
        // ${mapperClass}:sample.mbg.mapper.TTab01Mapper
        // mapper = sess.getMapper(sample.mbg.mapper.TTab01Mapper.class);
    }

    /**
     * コンストラクタ
     */
    // ${tableClass}:TTab01
    public TTab01Steps() throws Exception {
        //NOTE: @BeforeSuiteで取得しているのでコメントにする.
        // beforeSuite();
    }

    /**
     * テーブルの情報をキャッシュする
     * 検条件は少ないと想定されるためマージはしない.
     */
    // ${tableName}:T_TAB01 
    @Step("T_TAB01: 次の検索条件をキャッシュする。 <table>")
    public void cache(Table table) throws Exception, Throwable {
        //TODO: キャッシュの初期化はシナリオに移動する
        Map<String, List<List<Map<String, String>>>> caches = (Map<String, List<List<Map<String, String>>>>)SuiteDataStore.get("tables");
        if(caches == null){
            caches = new HashMap<String, List<List<Map<String, String>>>>();
            SuiteDataStore.put("tables", caches);
        }
        // ${tableClass}:TTab01
        List<List<Map<String, String>>> cache = caches.get("TTab01");
        if(cache == null){
            cache = new ArrayList();
            caches.put("TTab01", cache);
        }

        // ヘッダ情報マップ
        List<Map<String, String>> conditionMap = null;

        for(TableRow tableRow : table.getTableRows()){
            System.out.println("** cache: called.");
            // ヘッダ情報マップが未設定の場合
            if(conditionMap == null){
                // ヘッダ情報マップの取得
                conditionMap = getConditionMap(tableRow);
                // 1行目は条件のため次へ
                continue;
            }
            cache.add(getMap(conditionMap, tableRow));
            System.out.printf("** cache.size()=[%d].\n", cache.size());
        }
    }

    /**
     * 列名と条件のマップのリストを返す
     * TODO: 引数の数(0, 1, N)も返す.
     */
    List<Map<String, String>> getConditionMap(TableRow tableRow){
        List<Map<String, String>> list = new ArrayList();
        for(TableCell tableCell : tableRow.getTableCells()){
            // 列名
            String columnName = tableCell.getColumnName();
            // 検索条件
            String condition = tableCell.getValue();
            list.add(new HashMap(){{
                put("columnName", columnName);
                put("condition", getConditionMethodName(condition));
            }});
        }
        return list;
    }

    /**
     * 条件メソッド名の取得
     */
    String getConditionMethodName(String condition){
        switch(condition){
            case "<":
                return "isLessThanWhenPresent";
            case "<=":
                return "isLessThanOrEqualToWhenPresent";
            case ">=":
                return "isGreaterThanOrEqualToWhenPresent";
            case ">":
                return "isGreaterThanWhenPresent";
            case "like":
                return "isLikeWhenPresent";
            case "not like":
                return "isNotLikeWhenPresent";
            case "null":
                return "isNull";
            case "not null":
                return "isNotNull";
            case "in":
                return "isIn";
            case "not in":
                return "isNotIn";
        }
        return "isEqualToWhenPresent";
    }

    /**
     * TableRowからMapのリストを返す
     */
    List<Map<String, String>> getMap(List<Map<String, String>> conditionMap, TableRow tableRow) throws Exception, Throwable {
        Map<String, Map<String, String>> map2 = getMap2();
        List<Map<String, String>> list = new ArrayList();

        int idx = -1;
        for(TableCell tableCell : tableRow.getTableCells()){
            idx++;
            String condition = conditionMap.get(idx).get("condition"); 
            list.add(new HashMap(){{
                String columnName = tableCell.getColumnName();
                put("columnName", columnName);
                put("value", tableCell.getValue());
                put("condition", condition);
                put("className", map2.get(columnName).get("className"));
                put("fieldName", map2.get(columnName).get("fieldName"));
            }});
        }
        return list;
    }

    /**
     * MBGからの情報を取得する.
     * DB列名(D_NUM01)に紐づくJavaフィールド名(dNum01)、Javaクラス名(jara.math.BigDecimal)のマップ情報を取得する.
     * FIXED:gauge実行時に上記の情報が取得(特にJavaクラス名)できるようならそっちに置き換える
     */
    Map<String, Map<String, String>> getMap2() throws Throwable {
        Map<String, Map<String, String>> map = new HashMap();

        // ${dssClass}:sample.mbg.mapper.TTab01DynamicSqlSupport
        // ${tableClass}:TTab01
        Field[] fields = sample.mbg.mapper.TTab01DynamicSqlSupport.TTab01.class.getDeclaredFields();
        sample.mbg.mapper.TTab01DynamicSqlSupport.TTab01 tab = new sample.mbg.mapper.TTab01DynamicSqlSupport.TTab01();
        for(Field f : fields){
            if(f.getType() == SqlColumn.class){
                // Javaフィールド名
                String fieldName = f.getName();
                // Javaクラス名
                Type type = f.getGenericType();
                ParameterizedType parameterizedType = (ParameterizedType) type;
                Type[] actualTypeArguments = parameterizedType.getActualTypeArguments();
                Class<?> genericClass0 = (Class<?>) actualTypeArguments[0];
                String className = genericClass0.getName();
                // DB列名
                f.setAccessible(true);
                //TODO: tableClassだとダメのケースあり.x:Tab01 o:tab01
                // SqlColumn sc = (SqlColumn)f.get(sample.mbg.mapper.TTab01DynamicSqlSupport.TTab01);
                SqlColumn sc = (SqlColumn)f.get(tab);
                String columnName = sc.name();
                System.out.printf("** %s, %s, %s.\n", fieldName, className, columnName);
                map.put(columnName, new HashMap(){{
                    put("colName", columnName);
                    put("fieldName", fieldName);
                    put("className", className);
                }});
            }
        }
        return map;
    }

    /**
     * CSVファイルの出力する
     * FIXED: CSVファイルに出力する
     */
    // ${tableName}:T_TAB01
    @Step("T_TAB01: 出力する。")
    public void print() throws Exception {
        Map<String, List<List<Map<String, String>>>> caches = (Map<String, List<List<Map<String, String>>>>)SuiteDataStore.get("tables");
        List<List<Map<String, String>>> cache = caches.get("TTab01");

        System.out.printf("** cache.size()=[%d].\n", cache.size());
        System.out.println(cache);
        // selectList.apply(cache).apply(mapper)
        //   .forEach(System.out::println)
        // ;
        printCsv(selectList.apply(cache).apply(mapper));
    }


    /**
     * CSVファイルの出力する
     * FIXED: CSVファイルに出力する
     * TODO: CSVファイル名は外だしにする
     */
    // ${modelClass}:sample.mbg.model.TTab01
    void printCsv(List<sample.mbg.model.TTab01> list) throws Exception{
        CsvMapper csvMapper = new CsvMapper();
        CsvSchema csvSchema = csvMapper.schemaFor(sample.mbg.model.TTab01.class);
        //TODO: CSVファイル名は外だしにする
        BufferedWriter bufferedWriter = Files.newBufferedWriter(Paths.get("./my.T_TAB01.csv"));
        SequenceWriter sequenceWriter = csvMapper
            .setDateFormat(new SimpleDateFormat("yyyy/MM/dd HH:mm:ss"))
            .writerFor(Map.class)
            .with(csvSchema)
            .writeValues(bufferedWriter)
        ;
        list.forEach(e->{
            try{
                sequenceWriter.write(csvMapper.convertValue(e, Map.class));
            }catch(Exception ex){
                ex.printStackTrace();
            }
        });
    }

    /**
     * 検索条件とマッパを引数として結果一覧を返す関数.
     * @params conds TableCellから情報を取得した１行分のMap
     * @params mapper MSGで作成したマッパ
     * @return List 結果一覧
     * TODO: 1引数以外の条件にも対応する ※難問！？
     *  - m.invoke()の引数は配列で渡せるのか？
     *  - 引数の数によって戻り値の型が違うので明示的なキャストができない.分岐が必要なのか？
     */
    Function<List<List<Map<String, String>>>, Function<sample.mbg.mapper.TTab01Mapper, List<sample.mbg.model.TTab01>>> selectList = conditions-> mapper -> {
        List<sample.mbg.model.TTab01> list = mapper.select(dsl->{
            QueryExpressionWhereBuilder builder = dsl.where();
            MethodHandles.Lookup lookup = MethodHandles.lookup();
            try{
                for(List<Map<String, String>> row: conditions){
                    List<AndOrCriteriaGroup> cond= new ArrayList();

                    for(Map<String, String> c: row){
                        if(StringUtils.isEmpty(c.get("value"))) continue;

                        // ${dssClass}:sample.mbg.mapper.TTab01DynamicSqlSupport
                        Field f = sample.mbg.mapper.TTab01DynamicSqlSupport.class.getDeclaredField(c.get("fieldName"));
                        SqlColumn<?> sqlColumn = (SqlColumn)f.get(null);

                        Method m = getMethod(c.get("condition"));
                        Object[] vals = myConv(c);
                        // System.out.printf("** val.getClass=[%s].\n", vals.getClass());
                        // java.util.Arrays.stream(vals).forEach(System.out::println);
                        // java.util.Arrays.stream(m.getParameterTypes()).forEach(System.out::println);

                        MethodType methodType = MethodType.methodType(m.getReturnType(), m.getParameterTypes());
                        MethodHandle mh = lookup.findStatic(SqlBuilder.class, c.get("condition"), methodType);

                        cond.add(and(
                            (SqlColumn)sqlColumn,
                            (VisitableCondition)mh.invoke(m.getParameterTypes().length == 1 ? vals[0] : vals)
                        ));
                    }
                    builder.or(cond);
                }
                return builder;
            }catch(Exception e){ 
                e.printStackTrace();
                return builder; 
            }catch(Throwable e){ 
                e.printStackTrace();
                return builder; 
            }
        });
        return list;
    };

    //
    Method getMethod(String methodName) throws Exception {
        for(Method m : SqlBuilder.class.getDeclaredMethods()){
            if(!methodName.equals(m.getName())) continue;
            if(m.getParameters().length == 0) return m;

            boolean isFound = true;
            for(Parameter p : m.getParameters()){
                // System.out.printf("** p.getParameterizedType().getTypeName=[%s].\n", p.getParameterizedType().getTypeName());
                if(!"T".equals(p.getParameterizedType().getTypeName()) && !"T[]".equals(p.getParameterizedType().getTypeName()) ) {
                    isFound = false;
                    break;
                }
            }
            if(isFound){
                System.out.printf("** m.getName=[%s].\n", m.getName());
                return m;
            } 
        }
        // return null;
        throw new Exception(methodName + " not fouind.");
    }


    /**
     * クラス名に対応した型の値を取得する
     * FIXED: swtich文に変更する
     */
    Object[] myConv(Map<String, String> map) throws Exception {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        String type = map.get("className");
        String value = map.get("value");
         List<String> values = java.util.Arrays.asList(value.split(","));

        // 0引数の場合
        if("is null".equals(map.get("condition"))) return null;
        if("is not null".equals(map.get("condition"))) return null;

        switch(type){
            case "java.math.BigDecimal":
                return values.stream().map(v->new BigDecimal(v.trim())).collect(Collectors.toList()).toArray(new BigDecimal[values.size()]);
            case "java.util.Data":
                return values.stream().map(v->{
                    try{
                        return sdf.parse(v.trim());
                    }catch(Throwable e){
                        return null;
                    }
                }).collect(Collectors.toList()).toArray(new java.util.Date[values.size()]);
        }
        return values.stream().map(v->v.trim()).collect(Collectors.toList()).toArray(new String[values.size()]);
    }

}
