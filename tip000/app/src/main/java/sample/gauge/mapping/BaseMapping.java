package sample.gauge.mapping;

import java.lang.reflect.Method;

import org.mapstruct.Mapper;
import org.mapstruct.MappingTarget;
import org.mapstruct.ReportingPolicy;

import com.google.common.base.CaseFormat;
import com.thoughtworks.gauge.TableCell;
import com.thoughtworks.gauge.TableRow;

import sample.gauge.step.CommonSteps;

@Mapper(imports = sample.gauge.step.CommonSteps.class, unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface BaseMapping {

    default <T> T from(TableRow row, @MappingTarget T target) throws Exception {
        Class clazz = target.getClass();

        for(TableCell cell : row.getTableCells()){
            String colName = cell.getColumnName();
            String camelCaseName = CaseFormat.UPPER_UNDERSCORE.to(CaseFormat.UPPER_CAMEL, colName);
            String methodName = String.format("set%s", camelCaseName);

            Method[] methods = clazz.getDeclaredMethods();
            Method   method = null;
            for(int i=0; i<methods.length; i++){
                method = methods[i];
                if(method.getName().equals(methodName)) break;
            }
            if(method == null) continue;

            Class[] args = method.getParameterTypes();
            if(args.length == 0) continue;

            method.setAccessible(true);
            String arg0 = args[0].getName();
            if(arg0.contains("BigDecimal")) {
                method.invoke(target, CommonSteps.toBigDecimal(row, cell.getColumnName()));
            }else if(arg0.contains("Short")) {
                method.invoke(target, CommonSteps.toShort(row, cell.getColumnName()));
            }else if(arg0.contains("Long")) {
                method.invoke(target, CommonSteps.toLong(row, cell.getColumnName()));
            }else if(arg0.contains("Date")) {
                method.invoke(target, CommonSteps.toDate(row, cell.getColumnName()));
            }else{
                method.invoke(target, CommonSteps.toStr(row, cell.getColumnName()));
            }
        }
        return target;
    }
    
}
