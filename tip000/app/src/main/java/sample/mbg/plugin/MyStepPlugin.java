package sample.mbg.plugin;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import org.mybatis.generator.api.GeneratedJavaFile;
import org.mybatis.generator.api.IntrospectedTable;
import org.mybatis.generator.api.PluginAdapter;
import org.mybatis.generator.api.IntrospectedColumn;

import freemarker.cache.ClassTemplateLoader;
import freemarker.template.Configuration;
import freemarker.template.Template;

public class MyStepPlugin extends PluginAdapter {

    private Properties props;

    static Configuration config;
    static {
        config = new Configuration(Configuration.VERSION_2_3_32);
	    config.setDefaultEncoding("utf-8");
	    config.setTemplateLoader(new ClassTemplateLoader(MyStepPlugin.class, "/template"));    // src/test/resources/template/
	    // DefaultObjectWraper ow = (DefaultObjectWraper)config.getObjectWrapper();
    }

    @Override
    public void setProperties(Properties props){
        this.props = props;
    }

    @Override
    public boolean validate(List<String> list){
        return true;
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<GeneratedJavaFile> contextGenerateAdditionalJavaFiles(IntrospectedTable table){
        Template template = null;
    	try{
            template = config.getTemplate("myStep.ftl");
    	    StringWriter sw = new StringWriter();
    	    Map<String, Object> map = new HashMap<String, Object>(){{
                    put("modelClass",  table.getBaseRecordType());                              // sample.mbg.model.MHogeTbl
                    put("mapperClass", table.getMyBatis3JavaMapperType());                      // sample.mbg.mapper.MHogeTblMapper
                    put("dssClass",    table.getMyBatisDynamicSqlSupportType());                // sample.mbg.mapper.MHogeTblDynamicSqlSupport
                    put("tableName",   table.getFullyQualifiedTableNameAtRuntime());            // M_HOGE_TBL
                    put("pk",          table.getPrimaryKeyColumns());                           // NUM01 
                    put("columns",     table.getAllColumns());                                  // NUM01 CHR01, DAT01, ...
                    put("tableClass",  table.getFullyQualifiedTable().getDomainObjectName());   // MHogeTbl
                    put("package",     props.getProperty("targetPackage"));                 // sample.step
    	    }};
            // System.out.println(map);
            // ((List<IntrospectedColumn>) map.get("columns")).forEach(
            //     e->System.out.println(e.getFullyQualifiedJavaType().getFullyQualifiedNameWithoutTypeParameters())
            // );

    	    template.process(map, sw);
    	    String path = this.props.getProperty("targetProject") + "/"                                       // src/test/java
    	        + this.props.getProperty("targetPackage").replaceAll("\\.", "/") + "/"      // sample.step
    		    + table.getMyBatisDynamicSQLTableObjectName() + "Steps.java"                                      // MHogeTbl
            ;
    	    PrintWriter pw = new PrintWriter(path, StandardCharsets.UTF_8);
    	    pw.write(sw.toString());
    	    pw.flush();
    	    pw.close();
    	}catch(Exception ex){
            ex.printStackTrace();
	    }
        return new ArrayList<GeneratedJavaFile>();
    }

}
