package sample.mbg.mapper;

import static sample.mbg.mapper.TTab01DynamicSqlSupport.*;

import java.util.Collection;
import java.util.List;
import java.util.Optional;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.ResultMap;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.SelectProvider;
import org.apache.ibatis.type.JdbcType;
import org.mybatis.dynamic.sql.BasicColumn;
import org.mybatis.dynamic.sql.delete.DeleteDSLCompleter;
import org.mybatis.dynamic.sql.select.CountDSLCompleter;
import org.mybatis.dynamic.sql.select.SelectDSLCompleter;
import org.mybatis.dynamic.sql.select.render.SelectStatementProvider;
import org.mybatis.dynamic.sql.update.UpdateDSL;
import org.mybatis.dynamic.sql.update.UpdateDSLCompleter;
import org.mybatis.dynamic.sql.update.UpdateModel;
import org.mybatis.dynamic.sql.util.SqlProviderAdapter;
import org.mybatis.dynamic.sql.util.mybatis3.CommonCountMapper;
import org.mybatis.dynamic.sql.util.mybatis3.CommonDeleteMapper;
import org.mybatis.dynamic.sql.util.mybatis3.CommonInsertMapper;
import org.mybatis.dynamic.sql.util.mybatis3.CommonUpdateMapper;
import org.mybatis.dynamic.sql.util.mybatis3.MyBatis3Utils;
import sample.mbg.model.TTab01;

@Mapper
public interface TTab01Mapper extends CommonCountMapper, CommonDeleteMapper, CommonInsertMapper<TTab01>, CommonUpdateMapper {
    BasicColumn[] selectList = BasicColumn.columnList(dNum01, dChr01, dDat01);

    @SelectProvider(type=SqlProviderAdapter.class, method="select")
    @Results(id="TTab01Result", value = {
        @Result(column="D_NUM01", property="dNum01", jdbcType=JdbcType.NUMERIC),
        @Result(column="D_CHR01", property="dChr01", jdbcType=JdbcType.VARCHAR),
        @Result(column="D_DAT01", property="dDat01", jdbcType=JdbcType.TIMESTAMP)
    })
    List<TTab01> selectMany(SelectStatementProvider selectStatement);

    @SelectProvider(type=SqlProviderAdapter.class, method="select")
    @ResultMap("TTab01Result")
    Optional<TTab01> selectOne(SelectStatementProvider selectStatement);

    default long count(CountDSLCompleter completer) {
        return MyBatis3Utils.countFrom(this::count, TTab01, completer);
    }

    default int delete(DeleteDSLCompleter completer) {
        return MyBatis3Utils.deleteFrom(this::delete, TTab01, completer);
    }

    default int insert(TTab01 row) {
        return MyBatis3Utils.insert(this::insert, row, TTab01, c ->
            c.map(dNum01).toProperty("dNum01")
            .map(dChr01).toProperty("dChr01")
            .map(dDat01).toProperty("dDat01")
        );
    }

    default int insertMultiple(Collection<TTab01> records) {
        return MyBatis3Utils.insertMultiple(this::insertMultiple, records, TTab01, c ->
            c.map(dNum01).toProperty("dNum01")
            .map(dChr01).toProperty("dChr01")
            .map(dDat01).toProperty("dDat01")
        );
    }

    default int insertSelective(TTab01 row) {
        return MyBatis3Utils.insert(this::insert, row, TTab01, c ->
            c.map(dNum01).toPropertyWhenPresent("dNum01", row::getdNum01)
            .map(dChr01).toPropertyWhenPresent("dChr01", row::getdChr01)
            .map(dDat01).toPropertyWhenPresent("dDat01", row::getdDat01)
        );
    }

    default Optional<TTab01> selectOne(SelectDSLCompleter completer) {
        return MyBatis3Utils.selectOne(this::selectOne, selectList, TTab01, completer);
    }

    default List<TTab01> select(SelectDSLCompleter completer) {
        return MyBatis3Utils.selectList(this::selectMany, selectList, TTab01, completer);
    }

    default List<TTab01> selectDistinct(SelectDSLCompleter completer) {
        return MyBatis3Utils.selectDistinct(this::selectMany, selectList, TTab01, completer);
    }

    default int update(UpdateDSLCompleter completer) {
        return MyBatis3Utils.update(this::update, TTab01, completer);
    }

    static UpdateDSL<UpdateModel> updateAllColumns(TTab01 row, UpdateDSL<UpdateModel> dsl) {
        return dsl.set(dNum01).equalTo(row::getdNum01)
                .set(dChr01).equalTo(row::getdChr01)
                .set(dDat01).equalTo(row::getdDat01);
    }

    static UpdateDSL<UpdateModel> updateSelectiveColumns(TTab01 row, UpdateDSL<UpdateModel> dsl) {
        return dsl.set(dNum01).equalToWhenPresent(row::getdNum01)
                .set(dChr01).equalToWhenPresent(row::getdChr01)
                .set(dDat01).equalToWhenPresent(row::getdDat01);
    }
}