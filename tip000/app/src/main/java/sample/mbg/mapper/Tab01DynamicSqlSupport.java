package sample.mbg.mapper;

import java.sql.JDBCType;
import java.util.Date;
import org.mybatis.dynamic.sql.AliasableSqlTable;
import org.mybatis.dynamic.sql.SqlColumn;

public final class Tab01DynamicSqlSupport {
    public static final Tab01 tab01 = new Tab01();

    public static final SqlColumn<Short> num01 = tab01.num01;

    public static final SqlColumn<String> chr01 = tab01.chr01;

    public static final SqlColumn<Date> dat01 = tab01.dat01;

    public static final class Tab01 extends AliasableSqlTable<Tab01> {
        public final SqlColumn<Short> num01 = column("NUM01", JDBCType.NUMERIC);

        public final SqlColumn<String> chr01 = column("CHR01", JDBCType.VARCHAR);

        public final SqlColumn<Date> dat01 = column("DAT01", JDBCType.TIMESTAMP);

        public Tab01() {
            super("TAB01", Tab01::new);
        }
    }
}