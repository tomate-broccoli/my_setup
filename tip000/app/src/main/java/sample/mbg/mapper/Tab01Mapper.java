package sample.mbg.mapper;

import static sample.mbg.mapper.Tab01DynamicSqlSupport.*;

import java.util.Collection;
import java.util.List;
import java.util.Optional;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.ResultMap;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.SelectProvider;
import org.apache.ibatis.type.JdbcType;
import org.mybatis.dynamic.sql.BasicColumn;
import org.mybatis.dynamic.sql.delete.DeleteDSLCompleter;
import org.mybatis.dynamic.sql.select.CountDSLCompleter;
import org.mybatis.dynamic.sql.select.SelectDSLCompleter;
import org.mybatis.dynamic.sql.select.render.SelectStatementProvider;
import org.mybatis.dynamic.sql.update.UpdateDSL;
import org.mybatis.dynamic.sql.update.UpdateDSLCompleter;
import org.mybatis.dynamic.sql.update.UpdateModel;
import org.mybatis.dynamic.sql.util.SqlProviderAdapter;
import org.mybatis.dynamic.sql.util.mybatis3.CommonCountMapper;
import org.mybatis.dynamic.sql.util.mybatis3.CommonDeleteMapper;
import org.mybatis.dynamic.sql.util.mybatis3.CommonInsertMapper;
import org.mybatis.dynamic.sql.util.mybatis3.CommonUpdateMapper;
import org.mybatis.dynamic.sql.util.mybatis3.MyBatis3Utils;
import sample.mbg.model.Tab01;

@Mapper
public interface Tab01Mapper extends CommonCountMapper, CommonDeleteMapper, CommonInsertMapper<Tab01>, CommonUpdateMapper {
    BasicColumn[] selectList = BasicColumn.columnList(num01, chr01, dat01);

    @SelectProvider(type=SqlProviderAdapter.class, method="select")
    @Results(id="Tab01Result", value = {
        @Result(column="NUM01", property="num01", jdbcType=JdbcType.NUMERIC),
        @Result(column="CHR01", property="chr01", jdbcType=JdbcType.VARCHAR),
        @Result(column="DAT01", property="dat01", jdbcType=JdbcType.TIMESTAMP)
    })
    List<Tab01> selectMany(SelectStatementProvider selectStatement);

    @SelectProvider(type=SqlProviderAdapter.class, method="select")
    @ResultMap("Tab01Result")
    Optional<Tab01> selectOne(SelectStatementProvider selectStatement);

    default long count(CountDSLCompleter completer) {
        return MyBatis3Utils.countFrom(this::count, tab01, completer);
    }

    default int delete(DeleteDSLCompleter completer) {
        return MyBatis3Utils.deleteFrom(this::delete, tab01, completer);
    }

    default int insert(Tab01 row) {
        return MyBatis3Utils.insert(this::insert, row, tab01, c ->
            c.map(num01).toProperty("num01")
            .map(chr01).toProperty("chr01")
            .map(dat01).toProperty("dat01")
        );
    }

    default int insertMultiple(Collection<Tab01> records) {
        return MyBatis3Utils.insertMultiple(this::insertMultiple, records, tab01, c ->
            c.map(num01).toProperty("num01")
            .map(chr01).toProperty("chr01")
            .map(dat01).toProperty("dat01")
        );
    }

    default int insertSelective(Tab01 row) {
        return MyBatis3Utils.insert(this::insert, row, tab01, c ->
            c.map(num01).toPropertyWhenPresent("num01", row::getNum01)
            .map(chr01).toPropertyWhenPresent("chr01", row::getChr01)
            .map(dat01).toPropertyWhenPresent("dat01", row::getDat01)
        );
    }

    default Optional<Tab01> selectOne(SelectDSLCompleter completer) {
        return MyBatis3Utils.selectOne(this::selectOne, selectList, tab01, completer);
    }

    default List<Tab01> select(SelectDSLCompleter completer) {
        return MyBatis3Utils.selectList(this::selectMany, selectList, tab01, completer);
    }

    default List<Tab01> selectDistinct(SelectDSLCompleter completer) {
        return MyBatis3Utils.selectDistinct(this::selectMany, selectList, tab01, completer);
    }

    default int update(UpdateDSLCompleter completer) {
        return MyBatis3Utils.update(this::update, tab01, completer);
    }

    static UpdateDSL<UpdateModel> updateAllColumns(Tab01 row, UpdateDSL<UpdateModel> dsl) {
        return dsl.set(num01).equalTo(row::getNum01)
                .set(chr01).equalTo(row::getChr01)
                .set(dat01).equalTo(row::getDat01);
    }

    static UpdateDSL<UpdateModel> updateSelectiveColumns(Tab01 row, UpdateDSL<UpdateModel> dsl) {
        return dsl.set(num01).equalToWhenPresent(row::getNum01)
                .set(chr01).equalToWhenPresent(row::getChr01)
                .set(dat01).equalToWhenPresent(row::getDat01);
    }
}