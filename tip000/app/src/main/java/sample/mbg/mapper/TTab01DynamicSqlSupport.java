package sample.mbg.mapper;

import java.sql.JDBCType;
import java.util.Date;
import org.mybatis.dynamic.sql.AliasableSqlTable;
import org.mybatis.dynamic.sql.SqlColumn;

public final class TTab01DynamicSqlSupport {
    public static final TTab01 TTab01 = new TTab01();

    public static final SqlColumn<Short> dNum01 = TTab01.dNum01;

    public static final SqlColumn<String> dChr01 = TTab01.dChr01;

    public static final SqlColumn<Date> dDat01 = TTab01.dDat01;

    public static final class TTab01 extends AliasableSqlTable<TTab01> {
        public final SqlColumn<Short> dNum01 = column("D_NUM01", JDBCType.NUMERIC);

        public final SqlColumn<String> dChr01 = column("D_CHR01", JDBCType.VARCHAR);

        public final SqlColumn<Date> dDat01 = column("D_DAT01", JDBCType.TIMESTAMP);

        public TTab01() {
            super("T_TAB01", TTab01::new);
        }
    }
}