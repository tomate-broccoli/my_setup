package sample.mbg.step;

import static org.assertj.core.api.Assertions.*;
import static org.mybatis.dynamic.sql.SqlBuilder.*;
import static sample.mbg.mapper.TTab01DynamicSqlSupport.*;

// import java.util.function.BiConsumer;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import java.util.function.Function;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.lang.reflect.Field;
import java.lang.reflect.Method;

import com.thoughtworks.gauge.Step;
import com.thoughtworks.gauge.Table;
import com.thoughtworks.gauge.TableRow;
import com.thoughtworks.gauge.TableCell;
import com.thoughtworks.gauge.datastore.SuiteDataStore;

import sample.gauge.step.BaseSteps;
import org.mybatis.dynamic.sql.select.CountDSL.CountWhereBuilder;
import org.mybatis.dynamic.sql.delete.DeleteDSL.DeleteWhereBuilder;
import org.mybatis.dynamic.sql.select.QueryExpressionDSL.QueryExpressionWhereBuilder;
import org.mybatis.dynamic.sql.AndOrCriteriaGroup;
import org.mybatis.dynamic.sql.SqlBuilder;

public class TTab01Steps extends BaseSteps<sample.mbg.model.TTab01, sample.mbg.mapper.TTab01Mapper> {

    // コンストラクタ
    public TTab01Steps() throws Exception {
        super();
    }

    @Step("T_TAB01: 次のデータをキャッシュする。 <table>")
    public void cache(Table table) throws Exception {
        cache2(table);
    }

    // MD表のキャッシュ
    public void cache2(Table table) throws Exception {
        Map<String, List<Map<String, String>>> map = (Map<String, List<Map<String, String>>>)SuiteDataStore.get("tables");
        List<Map<String, String>> list = map.get("TTab01");
        if(list == null){
            list = new ArrayList();
            map.put("TTab01", list);
        }

        Map<Integer, Map<String, String>> conditionMap = null;
        for(TableRow tableRow : table.getTableRows()){
            if(conditionMap == null){
                conditionMap = getConditionMap(tableRow);
                continue;
            }
            list.add(getMap(conditionMap, tableRow));
        }
       //  map.put("TTab01", list);
    }

    // 列位置(0..)をキーとして列名と条件のマップを返す
    Map<Integer, Map<String, String>> getConditionMap(TableRow tableRow){
        Map<Integer, Map<String, String> map = new HashMap();
        int idx = -1;
        for(TableCell tableCell : tableRow.getTableCells()){
            String columnName = tableCell.getColumnName();
            String condition = tableCell.getValue();
            Map<String, String> m = new HashMap(){{
                m.put("columnName", columnName);
                m.put("condition", condition);
            }};
            map.put(++idx, m);
        }
        return map;
    }

    // TableRowからMapを返す
    Map<String, String> getMap(Map<Integer, Map<String, String>> conditionMap, TableRow tableRow){
        Map<String, String> map = new HashMap();
        int idx = -1;
        for(TableCell tableCell : tableRow.getTableCells()){
            idx++;
            String colName = tableCell.getColumnName();
            map.put("columnName", colName);
            // map.put("className", getArgs0AsString(TTab01.class, colName));
            map.put("value", tableCell.getValue());
            map.put("condition", conditionMap.get(idx).get("condition"));
        }
    }
/*
    // Java型の取得
    String geArgs0AsString(Class type, String fieldName){
        Method[] methods = type.getDeclaredMethods();
        Method method = methods.find(m->{
            String methodName = String.format("set%s", fieldName);
            return e.getName().equals(methodName) ? true : false;
        });
        Class[] args = method.getParameterTypes();
        method.setAccessible(true);
        return args[0].getName();
    }
*/
    @Step("T_TAB01: 出力する。")
    public void print() throws Exception {
        // printCache(selectList);
        selectList.apply(null).apply(null);
    }

    //
    Map<String, String> getMap2(){
        Map<String, Map<String, String>> map = new HashMap();
        map.put("D_NUM01", new HashMap<String, String>(){{
            put("colName", "D_NUM01");
            put("fieldName", "dNum01");
            put("className", "java.lang.Short");
        }});
        map.put("D_CHR01", new HashMap<String, String>(){{
            put("colName", "D_CHR01");
            put("fieldName", "dChr01");
            put("className", "java.lang.String");
        }});
        map.put("D_DAT01", new HashMap<String, String>(){{
            put("colName", "D_DAT01");
            put("fieldName", "dDat01");
            put("className", "java.util.Date");
        }});
        return map;
    }

    /**
     * 検索条件とマッパを引数として結果一覧を返す関数.
     *
     * condsは以下の情報をもつMapを要素とするリスト(行)を要素とするリスト: List<List<Map>>
     * ・columName : DB列名 D_NUM01
     * ・fieldName : dNum01
     * ・condision: 条件 >= (or isEqualsTo)
     * ・value: 値 10
     * ・javaType: Java型 java.lang.Short
     * valueが空でない場合、条件に基づいた検索を行う.
     * 課題：
     * ・Java型と条件をどう扱うか？
     * ・and()のarg0を動的に設定するのは難しそう...
     *   reflectionで変数(dNum01)を取得できるのか？
     *   field.getType().cast(field.get(DSS.class))で行けるかも？
     * ・and()のarg1のisEqualTo()などのarg0のキャストを動的に設定するのは無理そう...
     *   Object型だと多様性に対応できない...
     *   field.getType().cast(myConv(field.get(hoge)))で行けるかも？
     *
     * @params conds TableCellから情報を取得した１行分のMap
     * @params mapper MSGで作成したマッパ
     * @return List 結果一覧
     */
    Function<List<List<Map<String, String>>>, Function<sample.mbg.mapper.TTab01Mapper, List<sample.mbg.model.TTab01>>> selectList = conditions-> mapper -> {
        List<sample.mbg.model.TTab01> list = mapper.select(dsl->{
            QueryExpressionWhereBuilder builder = dsl.where();

            for(List<Map<String, String>> row: conditions){
                List<AndOrCriteriaGroup> cond= new ArrayList();
                for(Map<String, String> c: row){
                    if(c.get("value") == null) continue;

                    Field f = sample.mbg.mapper.TTab01DynamicSqlSupport.class.getDeclaredField(c.get("fieldName"));
                    Method m = SqlBuilder.class.getDeclaredMethod(c.get("condition")); 
                    cond.add((QueryExpressionWhereBuilder)builder.and(
                        f.getType().cast(f.get(null)),
                        m.getReturnType().cast(m.invoke(Class.forName(c.get("javaType")).cast(myConv(c))))
                    ));
                }
                builder.or(cond);
            }
            return builder;
        });
        return list;
    }

    //
    Object myConv(Map<String, String> map){
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy/mm/dd HH:MM:SS");
        String type = map.get("javaType");
        String value = map.get("value");
        return "java.lang.Short".equals(type) 
            ? Short.valueOf(value)
            : "java.lang.Integer".equals(type)
            ? Integer.valueOf(value)
            : "java.math.BigDecimal".equals(type)
            ? new BigDecimal(value)
            : "java.util.Date".equals(type)
            ? sdf.parse(value)
            : value
        ;
    }

}
