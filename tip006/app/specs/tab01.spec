# tab01.spec

    $ gradle clean gauge -PspecsDir=specs/tab01.spec -Ptags="setup" --info

## sample
Tags: setup

*  次のデータをキャッシュする。

    | NUM01 | CHR01 | DAT01               | NUM01b | CHR01b |
    |-------|-------|---------------------|--------|--------|
    | 3     | ABC   | 2024/01/08 12:34:56 | 4      | XYZ    |

* TAB01: 次のデータが存在する。

    | NUM01 | CHR01 |
    |-------|-------|
    | 1     | abc   |
    | 2     | xyz   |

* TAB01: 次のデータが存在する。

    | NUM01  | CHR01  | DAT01 |
    |--------|--------|-------|
    |<NUM01> |<CHR01> |<DAT01>|
    |<NUM01b>|<CHR01b>|<DAT01>|

* TAB01: 次のデータを削除する。

    | NUM01  | CHR01  | DAT01 |
    |--------|--------|-------|
    |<NUM01> |<CHR01> |<DAT01>|
    |<NUM01b>|<CHR01b>|<DAT01>|

* TAB01: 次のデータを登録する。

    | NUM01  | CHR01  | DAT01 |
    |--------|--------|-------|
    |<NUM01> |<CHR01> |<DAT01>|
    |<NUM01b>|<CHR01b>|<DAT01>|

* 処理を確定する。    

## sample 2
Tags: csv

    $ gradle clean gauge -PspecsDir=specs/tab01.spec -Ptags="csv" --info

* TAB01: 次のデータをキャッシュする。

    | NUM01 | CHR01 |
    |-------|-------|
    | 3     | ABC   |

X TAB01: キャッシュデータを登録する。

* TAB01: 出力する。
