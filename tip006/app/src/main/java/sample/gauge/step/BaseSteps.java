package sample.gauge.step;

import java.lang.reflect.Method;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.function.BiConsumer;

import org.apache.ibatis.session.SqlSession;
import org.mapstruct.factory.Mappers;
import org.mybatis.dynamic.sql.select.SelectDSLCompleter;

import com.thoughtworks.gauge.Table;
import com.thoughtworks.gauge.TableRow;
import com.thoughtworks.gauge.datastore.SuiteDataStore;

import sample.gauge.mapping.BaseMapping;

public abstract class BaseSteps<E, M> {

    SqlSession sess;
    BaseMapping mapping;
    // E entity;
    // M mapper;

    public BaseSteps() throws Exception {
        sess = (SqlSession)SuiteDataStore.get("session");
        mapping = Mappers.getMapper(BaseMapping.class);
        // mapper = getNewMapper();
        // entity = getNewEntity();
    }

    public E getNewEntity() throws Exception {
        Class<?> c = this.getClass();
        Type t = c.getGenericSuperclass();
        ParameterizedType pt = (ParameterizedType)t;
        Type[] args = pt.getActualTypeArguments();
        Class<?> e = (Class<?>)args[0];
        return (E)e.getConstructor().newInstance();
    }

    public M getNewMapper() throws Exception {
        Class<?> c = this.getClass();
        Type t = c.getGenericSuperclass();
        ParameterizedType pt = (ParameterizedType)t;
        Type[] args = pt.getActualTypeArguments();
        Class<?> m = (Class<?>)args[1];
        return (M)sess.getMapper(m);
    }

    public void insert(Table table) throws Exception {
        M mapper = getNewMapper();
        Method m = mapper.getClass().getMethod("insert", getNewEntity().getClass());

        for(TableRow row : table.getTableRows()){
            m.invoke(mapper, mapping.from(row, getNewEntity()));
        }
    }

    public void delete(Table table, BiConsumer<E, M> f) throws Exception {
        M mapper = getNewMapper();
        for(TableRow row : table.getTableRows()){
            f.accept(mapping.from(row, getNewEntity()), mapper);
        }
    }

    public void count(Table table, BiConsumer<E, M> f) throws Exception {
        delete(table, f);
    }

    public void cache(Table table) throws Exception {
        Map<String, List<E>> map = (Map<String, List<E>>)SuiteDataStore.get("tables");
        String className = getNewEntity().getClass().getName();
        List<E> list = map.get(className);
        if(list == null){
            list = new ArrayList<E>();
            for(TableRow row : table.getTableRows()){
                list.add(mapping.from(row, getNewEntity()));
            }
        }else {
            int i = -1;
            for(TableRow row : table.getTableRows()){
                list.set(i, mapping.from(row, list.get(++i)));
            }
        }
        map.put(className, list);
    }

    public void insertCache() throws Exception {
        M mapper = getNewMapper();
        Map<String, List<E>> map = (Map<String, List<E>>)SuiteDataStore.get("tables");
        String className = getNewEntity().getClass().getName();
        List<E> list = map.get(className);

        Method m = mapper.getClass().getMethod("insert", getNewEntity().getClass());
        for(E e : list){
            m.invoke(mapper, e);
        }
    }

    public void printCache(BiConsumer<E, M> f) throws Exception {
        M mapper = getNewMapper();
        Map<String, List<E>> map = (Map<String, List<E>>)SuiteDataStore.get("tables");
        String className = getNewEntity().getClass().getName();
        List<E> list = map.get(className);

        // Method m = mapper.getClass().getMethod("select", SelectDSLCompleter.class);
        for(E e : list){
            // m.invoke(mapper, f);
            f.accept(e, mapper);
        }
    }
    
}
